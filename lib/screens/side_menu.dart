import 'package:flutter/material.dart';
import 'package:hidden_drawer_menu/hidden_drawer/hidden_drawer_menu.dart';
import 'package:talk_to_me/screens/discover.dart';
import 'package:talk_to_me/screens/home.dart';
import 'package:talk_to_me/screens/login_screen.dart';
import 'package:talk_to_me/screens/sign_up.dart';

class SideMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SimpleHiddenDrawer(
      menu: Menu(),
      screenSelectedBuilder: (position, controller) {
        Widget screenCurrent;
        switch (position) {
          case 0:
            screenCurrent = HomeScreen();
            break;
          case 1:
            screenCurrent = Discover();
            break;
        }

        return Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            leading: IconButton(
                icon: Icon(
                  Icons.menu,
                  color: Colors.black,
                ),
                onPressed: () {
                  controller.toggle();
                }),
          ),
          body: screenCurrent,
        );
      },
    );
  }
}

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> with TickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    super.initState();
  }

  @override
  void didChangeDependencies() {
    SimpleHiddenDrawerProvider.of(context)
        .getMenuStateListener()
        .listen((state) {
      if (state == MenuState.open) {
        _animationController.forward();
      }

      if (state == MenuState.closing) {
        _animationController.reverse();
      }
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      color: Colors.cyan,
      child: Stack(
        children: <Widget>[
          Container(
            width: double.maxFinite,
            height: double.maxFinite,
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [Color(0xff11F5E8), Color(0xffE3F9F7)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            )),
          ),
          Padding(
              padding: EdgeInsets.only(top: 100, left: 60),
              child: SizedBox(
                  height: 100,
                  width: 100,
                  child: Image.asset(
                    'assets/images/logo.png',
                    height: 100,
                    width: 100,
                  ))),
          FadeTransition(
            opacity: _animationController,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                        width: 200.0,
                        child: RichText(
                            text: TextSpan(
                          text: "Home",
                          style: TextStyle(color: Colors.white, fontSize: 22),
                        ))),
                    SizedBox(
                      width: 200.0,
                      child: RichText(
                          text: TextSpan(
                        text: "Top Most",
                        style: TextStyle(color: Colors.white, fontSize: 22),
                      )),
                    ),
                    SizedBox(
                        width: 200.0,
                        child: RichText(
                            text: TextSpan(
                          text: "Discover",
                          style: TextStyle(color: Colors.white, fontSize: 22),
                        ))),
                    SizedBox(
                        width: 200.0,
                        child: RichText(
                            text: TextSpan(
                          text: "Profile",
                          style: TextStyle(color: Colors.white, fontSize: 22),
                        ))),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
