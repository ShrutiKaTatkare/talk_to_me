import 'package:flutter/material.dart';
import 'package:spincircle_bottom_bar/modals.dart';
import 'package:spincircle_bottom_bar/spincircle_bottom_bar.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SpinCircleBottomBarHolder(
        bottomNavigationBar: SCBottomBarDetails(
            circleColors: [
              Color(0xff11F5E8),
              Color(0xffFBEC16),
              Color(0xffFF4AA1)
            ],
            iconTheme: IconThemeData(color: Colors.white),
            activeIconTheme: IconThemeData(color: Colors.white),
            backgroundColor: Color(0xffFF5F6D),
            titleStyle: TextStyle(color: Colors.white, fontSize: 12),
            activeTitleStyle: TextStyle(
                color: Colors.white, fontSize: 12, fontWeight: FontWeight.bold),
            actionButtonDetails: SCActionButtonDetails(
                color: Colors.white,
                icon: Icon(
                  Icons.expand_less,
                  color: Color(0xffFF5F6D),
                ),
                elevation: 2),
            elevation: 2.0,
            items: [
              // Suggested count : 4
              SCBottomBarItem(
                  icon: Icons.home, title: "Home", onPressed: () {}),
              SCBottomBarItem(
                  icon: Icons.mic, title: "Top Most", onPressed: () {}),
              SCBottomBarItem(
                  icon: Icons.list, title: "Discover", onPressed: () {}),
              SCBottomBarItem(
                  icon: Icons.person, title: "Profile", onPressed: () {}),
            ],
            circleItems: [
              //Suggested Count: 3
              // SCItem(icon: Icon(Icons.add), onPressed: () {}),
              // SCItem(icon: Icon(Icons.message), onPressed: () {}),
              SCItem(
                  icon: Icon(
                    Icons.message,
                    color: Colors.white,
                  ),
                  onPressed: () {}),
            ],
            bnbHeight: 80 // Suggested Height 80
            ),
        // Put your Screen Content in Child
        child: Column(
          children: [
            Center(
                child: Image.asset(
              'assets/images/logo.png',
              height: 150,
              width: 130,
            ))
          ],
        ),
      ),
    );
  }
}
