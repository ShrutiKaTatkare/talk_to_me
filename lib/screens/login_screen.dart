import 'package:flutter/material.dart';
import 'package:talk_to_me/screens/sign_up.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  GlobalKey<FormState> loginFormKey = new GlobalKey<FormState>();
  bool autoValidate = false;
  bool checked = false;
  validateForm() {
    if (loginFormKey.currentState.validate()) {
      Navigator.popAndPushNamed(context, '/menu');
    } else {
      setState(() {
        autoValidate = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(children: <Widget>[
        Positioned.fill(
          child: Image.asset(
            "assets/images/bottom_login.png",
            fit: BoxFit.fitWidth,
            alignment: Alignment.bottomLeft,
          ),
        ),
        Center(
            child: Form(
                key: loginFormKey,
                autovalidate: autoValidate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 80,
                    ),
                    Center(
                        child: Image.asset(
                      'assets/images/logo.png',
                      height: 30,
                    )),
                    SizedBox(
                      height: 50,
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'User name should not be empty';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person),
                                labelText: 'User Name'))),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Password should not be empty';
                              }
                              return null;
                            },
                            obscureText: true,
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.lock),
                                labelText: 'Password'))),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        child: Row(children: [
                          Checkbox(
                              value: checked,
                              onChanged: (value) {
                                setState(() {
                                  checked = value;
                                });
                              }),
                          Text(
                            'Login as Anonymous',
                            style: TextStyle(color: Colors.grey, fontSize: 18),
                          )
                        ])),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: 50.0,
                      width: MediaQuery.of(context).size.width / 1.6,
                      child: RaisedButton(
                        onPressed: validateForm,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        padding: EdgeInsets.all(0.0),
                        child: Ink(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [Color(0xff1FA2FF), Color(0xffA6FFCB)],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                              ),
                              borderRadius: BorderRadius.circular(30.0)),
                          child: Container(
                            constraints: BoxConstraints(
                                maxWidth: 300.0, minHeight: 50.0),
                            alignment: Alignment.center,
                            child: Text(
                              "Login",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        child: Row(
                          children: [
                            Expanded(
                                child: Container(
                              color: Colors.grey[300],
                              height: 2,
                            )),
                            Text(
                              '  OR  ',
                              style: TextStyle(color: Colors.grey[400]),
                            ),
                            Expanded(
                                child: Container(
                              height: 2,
                              color: Colors.grey[300],
                            )),
                          ],
                        )),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: 50.0,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => SignUp()),
                          );
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        padding: EdgeInsets.all(0.0),
                        child: Ink(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [Color(0xffFF5F6D), Color(0xffFFC371)],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                              ),
                              borderRadius: BorderRadius.circular(30.0)),
                          child: Container(
                            constraints: BoxConstraints(
                                maxWidth: 300.0, minHeight: 50.0),
                            alignment: Alignment.center,
                            child: Text(
                              "Create an account",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )))
      ]),
    );
  }
}
