import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:liquid_swipe/liquid_swipe.dart';
import 'package:talk_to_me/screens/login_screen.dart';

class Tutorials extends StatelessWidget {
  final pages = [
    Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [
            Color(0xff12FFF7),
            Color(0xff8CFFF9),
            Color(0xffDDFFFD),
          ],
          stops: [0.2, 0.5, 0.9],
        )),
        child: Center(
            child: Column(children: [
          SizedBox(
            height: 100,
          ),
          Image.asset(
            'assets/images/tutorial1.png',
          ),
          Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Want to share somthing ?',
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff1F1E1E)),
              )),
          Container(
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Text(
                'is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard  dummy text ever since the 1500s, when an unknown printer took a  galley of type and scrambled.',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 18, color: Color(0xff736B6B)),
              )),
          DotsIndicator(
            dotsCount: 3,
            position: 0,
            decorator: DotsDecorator(
              color: Colors.white,
              activeColor: Color(0xff0A1B45),
            ),
          )
        ]))),
    Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [Color(0xffFFA0CE), Color(0xffFFB7D9), Color(0xffFFDCED)],
          stops: [0.2, 0.5, 0.9],
        )),
        child: Center(
            child: Column(children: [
          SizedBox(
            height: 100,
          ),
          Image.asset(
            'assets/images/tutorial2.png',
          ),
          Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                'Ask Everything',
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff1F1E1E)),
              )),
          Container(
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Text(
                'is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard  dummy text ever since the 1500s, when an unknown printer took a  galley of type and scrambled.',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 18, color: Color(0xff736B6B)),
              )),
          DotsIndicator(
            dotsCount: 3,
            position: 1,
            decorator: DotsDecorator(
              color: Colors.white,
              activeColor: Color(0xff0A1B45),
            ),
          )
        ]))),
    Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: [Color(0xffFFF45C), Color(0xffFFF9AA), Color(0xffFFFCD5)],
        stops: [0.2, 0.5, 0.9],
      )),
      child: Center(
          child: Column(
        children: [
          SizedBox(
            height: 100,
          ),
          Image.asset(
            'assets/images/tutorial3.png',
            height: 150,
          ),
          Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              child: Text(
                'Talk To Me',
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff1F1E1E)),
              )),
          Container(
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Text(
                'is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard  dummy text ever since the 1500s, when an unknown printer took a  galley of type and scrambled.',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 18, color: Color(0xff736B6B)),
              )),
          DotsIndicator(
            dotsCount: 3,
            position: 2,
            mainAxisSize: MainAxisSize.min,
            decorator: DotsDecorator(
              color: Colors.white,
              activeColor: Color(0xff0A1B45),
            ),
          ),
        ],
      )),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          actions: [
            FlatButton(
                onPressed: () {
                  Navigator.popAndPushNamed(context, '/loginScreen');
                },
                child: Text('SKIP',
                    style: TextStyle(color: Colors.grey, fontSize: 18)))
          ],
        ),
        floatingActionButton: FloatingActionButton(
            mini: true,
            backgroundColor: Color(0xff0A1B45),
            onPressed: () {
              Navigator.popAndPushNamed(context, '/loginScreen');
            },
            child: Icon(
              Icons.arrow_forward_ios,
              size: 18,
            )),
        body: Center(
            child: Stack(children: [
          Builder(
              builder: (context) => LiquidSwipe(
                    pages: pages,
                    enableLoop: false,
                  ))
        ])));
  }
}
