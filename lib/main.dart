import 'package:flutter/material.dart';
import 'package:talk_to_me/screens/login_screen.dart';
import 'package:talk_to_me/screens/side_menu.dart';

import 'screens/tutorials.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Talk To Me',
      home: Tutorials(),
      routes: {
        '/menu': (context) => SideMenu(),
        '/loginScreen': (context) => LoginScreen()
      },
      // SplashScreen(),
    );
  }
}
